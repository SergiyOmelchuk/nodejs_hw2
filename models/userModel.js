const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    username: {
        type: String,
        require: true,
        uniq: true
    },
    password: {
        type: String,
        require: true
    },
    createData: {
        type: Date,
        default: Date.now()
    }
})

module.exports.User = mongoose.model('User', userSchema)
