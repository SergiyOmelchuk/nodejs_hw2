const mongoose = require('mongoose')

const noteSchema = mongoose.Schema({
    userId: {
        type: String,
        require: true,
    },
    text: {
        type: String,
        require: true,
    },
    completed: {
        type: Boolean,
        default: false,
    },
    createdDate: {
        type: Date,
        default: Date.now(),
    },
})

module.exports.Note = mongoose.model('Note', noteSchema)
