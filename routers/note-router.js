const express = require('express')
const router = express.Router()

const {asyncWrapper} = require('./helpers')
const {validateToken} = require('./middlewares/validate-token')
const { getNotes, addNote, getNoteById, updateNoteById, deleteNoteById, patchNoteById } = require('../controllers/notes-controller')

router.post('/', asyncWrapper(validateToken), asyncWrapper(addNote))
router.get('/', asyncWrapper(validateToken), asyncWrapper(getNotes))
router.get('/:id', asyncWrapper(validateToken), asyncWrapper(getNoteById))
router.put('/:id', asyncWrapper(validateToken), asyncWrapper(updateNoteById))
router.delete('/:id', asyncWrapper(validateToken), asyncWrapper(deleteNoteById))
router.patch('/:id', asyncWrapper(validateToken), asyncWrapper(patchNoteById))


module.exports = router