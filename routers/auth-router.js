const express = require('express')
const router = express.Router()

const {asyncWrapper} = require('./helpers')
const {validateRegistration} = require('./middlewares/validation-middlewares')
const {login, registration} = require('../controllers/auth-controller')

router.post('/register', asyncWrapper(validateRegistration), asyncWrapper(registration))
router.post('/login', asyncWrapper(validateRegistration), asyncWrapper(login))

module.exports = router
