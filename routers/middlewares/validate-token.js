const Joi = require('joi')
const jwt = require('jsonwebtoken')
const { JWT_SECRET } = require('../../config')

module.exports.validateToken = async (req, res, next) => {
    const header = req.headers['authorization'];
    if (!header) {
        return res.status(400).json({message: `No Authorization http header`});
    }

    const schema = Joi.string()
    try {
        await schema.validateAsync(header )
        const [tokenType, token] = header.split(' ');

        if (!token) {
            return res.status(401).json({message: `No JWT token found!`});
        }
        req.user = jwt.verify(token, JWT_SECRET)

    } catch (err) {
        return res.status(400).json({message: 'No valid JWT!'});
    }

    next()
}
