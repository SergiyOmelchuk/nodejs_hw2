const Joi = require('joi')
const {badRequestError} = require('../../errors')

module.exports.validateRegistration = async (req, res, next) => {
    const schema = Joi.object({
        username: Joi.string()
            .required(),
        password: Joi.string()
            // .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
    })

    try {
        await schema.validateAsync(req.body)
    } catch (err) {
        throw new badRequestError(err.message)
    }

    next()
}
