require('dotenv').config()
const express = require('express');
const morgan = require('morgan');
const mongoose = require("mongoose");
const port = process.env.PORT || 8080;
const { badRequestError } = require('./errors');

const authRouter = require('./routers/auth-router');
const userRouter = require('./routers/user-router');
const noteRouter = require('./routers/note-router');

const app = express();


app.use(express.json());
app.use(morgan('tiny'));
app.use(express.static('build'));

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/notes', noteRouter);




app.use((err, req, res, next) => {
    if (err instanceof badRequestError) {
        return res.status(err.statusCode).json({message: `${err.message}`})
    }

    return res.status(500).json({ message: err.message })
});

const start = async () => {
    await mongoose.connect('mongodb+srv://admin:adminpassword@cluster0.hu5vy.mongodb.net/db_test?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    })

    app.listen(port, () => {
        console.log(`Server works at port ${port}!`)
    })
}

start()
