const bcrypt = require("bcrypt");
const {User} = require('../models/userModel');
const {joiError} = require('../routers/middlewares/validation-middlewares')
const { badRequestError } = require('../errors')

const getUserProfile = async (req, res, next) => {
    const user = await User.findOne({_id : req.user._id})

    if(!user){
        throw new joiError('No user found')
    }

    res.status(200).json({user})

}

const deleteUser = async (req, res, next) => {
    const user = await User.findOne({_id : req.user._id});

    if(!user){
        throw new badRequestError('No user found');
    }

    await User.deleteOne({_id : req.user._id})

    res.status(200).json({message: 'User was deleted successfully'});
};
 const changeUserPasword = async (req, res, next) => {
    const {oldPassword, newPassword} = req.body;
    const user = await User.findOne({_id : req.user._id});


    if (!(await bcrypt.compare(oldPassword, user.password))) {
        throw new badRequestError('Old password is not correct');
    }

    if (newPassword === oldPassword) {
        throw new badRequestError('Passwords should not be same');
    }

    await User.updateOne(user, {
        $set: {
            password: await bcrypt.hash(newPassword, 10),
        },
    });

    res.status(200).json({message: 'Password was changed'});
};


module.exports = {
    getUserProfile,
    deleteUser,
    changeUserPasword
}
