const bcrypt = require('bcrypt')
const saltRounds = 10
const {User} = require('../models/userModel')
const jwt = require('jsonwebtoken')
const {JWT_SECRET} = require('../config')

module.exports.login = async (req, res) => {
    const {username, password} = req.body
    if (!username || !password ) {
        return res.status(400).json({message: `'Enter your credentials!`})
    }

    const user = await User.findOne({username})

    if (!user ) {
        return res.status(400).json({message: `No user with name - '${username}' found`})
    }
    if (!(await bcrypt.compare(password, user.password))) {
        return res.status(400).json({message: `Wrong password`})
    }

    const token = jwt.sign({username: user.username, _id: user._id}, JWT_SECRET)
    res.status(200).json({
        message: "Success",
        jwt_token: token
    })
}
module.exports.registration = async (req, res) => {
    const {username, password} = req.body
    if (!username) {
        return res.status(400).json({message: 'Enter user name'})
    }
    if (!password) {
        return res.status(400).json({message: 'Enter password'})
    }
    if ( await User.findOne({ username })) {
        return res.status(400).json({ message: 'User is already exist!' })
    }
    const user = new User({
        username,
        password: await bcrypt.hash(password, saltRounds)
    })

    await user.save()

    res.json({message: "Success"})
}
