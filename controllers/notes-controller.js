const {Note} = require('../models/noteModel')
const {User} = require('../models/userModel')
const {badRequestError} = require('../errors')

const addNote = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    console.log(user)

    const note = new Note({
        userId: user._id,
        text: req.body.text
    })

    await note.save()

    res.json({message: "Node added successfully"})
}

const getNotes = async (req, res, next) => {
    const {skip = 0, limit = 5} = req.query
    const requestOptions = {
        skip: parseInt(skip),
        limit: limit > 100 ? 5 : parseInt(limit),
    }
    const user = await User.findOne({_id: req.user._id})
    const notes = await Note.find({userId: user._id}, {__v: 0}, requestOptions )

    // if (notes.length === 0) {
    //     // throw new badRequestError(`No notes found`)
    //     return res.status(200).json({notes})
    // }
    res.status(200).json({notes});
}

const getNoteById = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const userId = req.user._id
    const _id = req.params['id']


    const note = await Note.findOne({userId, _id: req.params.id}, {__v: 0});

    if (!note) {
        throw new badRequestError('No notes find with your id')
    }
    req.note = note;

    res.status(200).json({note: req.note});
};

const updateNoteById = async (req, res, next) => {
    if (!req.body.text) {
        throw new badRequestError('No text')
    }
    const user = await User.findOne({_id: req.user._id})
    const userId = req.user._id
    const _id = req.params['id']

    const note = await Note.findOne({userId, _id: req.params.id}, {__v: 0});

    await Note.updateOne(note, req.body);

    res.status(200).json({message: 'Note was updated successfully'});
};

const deleteNoteById = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const userId = req.user._id
    const _id = req.params['id']

    const note = await Note.findOne({userId, _id: req.params.id}, {__v: 0});
    await Note.deleteOne(note);

    res.status(200).json({message: 'Note was deleted successfully'});
};

const patchNoteById = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const userId = req.user._id
    const _id = req.params['id']

    const note = await Note.findOne({userId, _id: req.params.id}, {__v: 0});
    await Note.updateOne(note, {completed: !note.completed});

    res.status(200).json({message: 'Your note status was changed successfully'});
};



module.exports = {
    addNote,
    getNotes,
    getNoteById,
    updateNoteById,
    deleteNoteById,
    patchNoteById
}
